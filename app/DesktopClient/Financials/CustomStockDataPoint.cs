﻿using AlphaVantage.Net.Stocks.TimeSeries;
using System;
using System.ComponentModel;

namespace DesktopClient.Financials
{
    public class CustomStockDataPoint : INotifyPropertyChanged
    {
        StockDataPoint stockDataPoint = new StockDataPoint();
        StockTimeSeries stockTimeTimeSeries = new StockTimeSeries();

        private string _percentageChange;
        public string PercentageChange
        {
            get => _percentageChange;
            set 
            { 
                _percentageChange = value;
                OnPropertyChanged(nameof(PercentageChange));
            }
        }

        public decimal Price
        {
            get => stockDataPoint.ClosingPrice;
            set
            { 
                stockDataPoint.ClosingPrice = value;
                OnPropertyChanged(nameof(Price));
            }
        }

        public decimal LowestPrice
        {
            get => stockDataPoint.LowestPrice;
            set
            {
                stockDataPoint.LowestPrice = value;
                OnPropertyChanged(nameof(LowestPrice));
            }
        }
        public decimal HighestPrice
        {
            get => stockDataPoint.HighestPrice;
            set
            {
                stockDataPoint.HighestPrice = value;
                OnPropertyChanged(nameof(HighestPrice));
            }
        }
        public string Symbol
        {
            get => stockTimeTimeSeries.Symbol;
            set
            {
                stockTimeTimeSeries.Symbol = value;
                OnPropertyChanged(nameof(HighestPrice));
            }
        }
        public DateTime TradingTime 
        {
            get => stockTimeTimeSeries.LastRefreshed;
            set
            {
                stockTimeTimeSeries.LastRefreshed = value;
                OnPropertyChanged(nameof(TradingTime));
            }
        }

        public void SetPrecisionTo(int precision)
        {
            stockDataPoint.OpeningPrice = Math.Round(stockDataPoint.OpeningPrice, precision, MidpointRounding.ToEven);
            stockDataPoint.ClosingPrice = Math.Round(stockDataPoint.ClosingPrice, precision, MidpointRounding.ToEven);
            stockDataPoint.LowestPrice  = Math.Round(stockDataPoint.LowestPrice,  precision, MidpointRounding.ToEven); 
            stockDataPoint.HighestPrice = Math.Round(stockDataPoint.HighestPrice, precision, MidpointRounding.ToEven);  
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
