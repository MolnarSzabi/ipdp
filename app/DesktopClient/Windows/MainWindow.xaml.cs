﻿using DesktopClient.ViewModel;
using System.Windows;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using AlphaVantage.Net.Stocks.TimeSeries;
using DesktopClient.AlphaVDataProvider;
using DesktopClient.Financials;
using ServiceStack;
using System.Data;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Input;
using System.Windows.Controls;
using Intrinio.SDK.Model;
using DesktopClient.UserData;

namespace DesktopClient
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
            TotalProfits.Text = "Total profits: " + TransactionProvider.CalculateProfits();
        }

        private void ResetZoom()
        {
            CartesianChart.AxisX[0].MinValue = double.NaN;
            CartesianChart.AxisX[0].MaxValue = double.NaN;
            CartesianChart.AxisY[0].MinValue = double.NaN;
            CartesianChart.AxisY[0].MaxValue = double.NaN;
        }

        private async Task StopPreviousChartUpdate(MainWindowViewModel viewModel)
        {
            await viewModel.StopChartUpdate();
        }

        private void UpdateInterfaceIntrinioData(MainWindowViewModel viewModel)
        {
            var selectedStockTicker = (stockList.SelectedItem as CustomStockDataPoint).Symbol;
            ApiResponseSecurityStockPrices savedStockData = viewModel.FindStockDataIntrinio(selectedStockTicker);
            if (savedStockData != null)
            {
                ResetZoom();
                viewModel.PlotData(savedStockData);
                viewModel.UpdateDataGridPartially(selectedStockTicker, stockList.SelectedItem); // partaial chart update with aplhavintage api beacause open, low and high price are null with intrinio intraday and real time requests
                viewModel.StartIntrinioChartUpdater(selectedStockTicker, stockList.SelectedItem);
            }
            else
            {
                Console.WriteLine("Stock data not found in saved stock list!");
            }
        }

        private void UpdateInterfaceAlphaVintageData(MainWindowViewModel viewModel)
        {
            var selectedStockTicker = (stockList.SelectedItem as CustomStockDataPoint).Symbol;
            StockTimeSeries savedStockData = viewModel.FindStockDataAlphaVintage(selectedStockTicker);
            if (savedStockData != null)
            {
                ResetZoom();
                viewModel.PlotData(savedStockData);
                viewModel.StartWindowDataUpdaterAlphaVantage(selectedStockTicker, stockList.SelectedItem);
            }
            else
            {
                Console.WriteLine("Stock data not found in saved stock list!");
            }
        }

        private void BuyStock(MainWindowViewModel viewModel)
        {
            var selectedStockTicker = (stockList.SelectedItem as CustomStockDataPoint).Symbol;
            if (selectedStockTicker != null)
            {
                UserTransaction transaction = TransactionProvider.GetUserTransactionsObject(selectedStockTicker, 100);
                TransactionProvider.SerializeTransactions(transaction);
                viewModel.AddTransactionToDataGrid(transaction);
                TotalProfits.Text = "Total profits: " + TransactionProvider.CalculateProfits();
                Console.WriteLine("Buy transaction");
            }
            else
            {
                Console.WriteLine("Stock data not found in saved stock list!");
            }
        }

        private void SellStock(MainWindowViewModel viewModel)
        {
            var selectedStockTicker = (stockList.SelectedItem as CustomStockDataPoint).Symbol;
            if (selectedStockTicker != null)
            {
                var transaction = TransactionProvider.GetUserTransactionsObject(selectedStockTicker, -100);
                TransactionProvider.SerializeTransactions(transaction);
                viewModel.AddTransactionToDataGrid(transaction);
                TotalProfits.Text = "Total profits: " + TransactionProvider.CalculateProfits();
                Console.WriteLine("Sell transaction");
            }
            else
            {
                Console.WriteLine("Stock data not found in saved stock list!");
            }
        }
        private async void StockList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (DataContext is MainWindowViewModel)
            {
                var viewModel = (MainWindowViewModel)DataContext;
                await StopPreviousChartUpdate(viewModel);
                if(viewModel.UseIntrinioApi)
                    UpdateInterfaceIntrinioData(viewModel);
                else
                    UpdateInterfaceAlphaVintageData(viewModel);
            }
        }

        private void BuyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DataContext is MainWindowViewModel)
                {
                    var viewModel = (MainWindowViewModel)DataContext;
                    if (viewModel.UseIntrinioApi)
                        BuyStock(viewModel);
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("No stock selected. Click on a stock from list!");
            }

        }

        private void SellButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DataContext is MainWindowViewModel)
                {
                    var viewModel = (MainWindowViewModel)DataContext;
                    if (viewModel.UseIntrinioApi)
                        SellStock(viewModel);
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("No stock selected. Click on a stock from list!");
            }

        }
    }
}
