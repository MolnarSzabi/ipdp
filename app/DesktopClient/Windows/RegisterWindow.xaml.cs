﻿using DesktopClient.ViewModel;
using System;
using System.Windows;

namespace DesktopClient.Windows
{
    public partial class RegisterWindow : Window
    {
        public RegisterWindow()
        {
            InitializeComponent();
            DataContext = new RegisterWindowViewModel();
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is RegisterWindowViewModel viewmodel)
            {
                if (viewmodel.DoRegister(UserPasswordBox.Password))
                {
                    var nextWindow = new LoginWindow();
                    Hide();
                    nextWindow.Show();
                }
                else { 
                    Console.WriteLine("Username already exists! Please choose another username.");
                    UserNameTextBox.Clear(); 
                }
            }
        }
    }
}
