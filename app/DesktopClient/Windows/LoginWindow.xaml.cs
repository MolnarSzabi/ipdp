﻿using DesktopClient.ViewModel;
using System;
using System.Windows;


namespace DesktopClient.Windows
{
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            DataContext = new LoginWindowViewModel();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is LoginWindowViewModel viewModel)
            {
                if (viewModel.DoLogin(UserPasswordBox.Password))
                {
                    var nextWindow = new MainWindow();
                    Hide();
                    nextWindow.Show();
                }
                else Console.WriteLine("User does not exist or wrong password!");
            }
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            var nextWindow = new RegisterWindow();
            Hide();
            nextWindow.Show();
        }
    }
}
