﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesktopClient.UserData
{
    public sealed class CurrentSession
    {
        public User User { get; set; }

        private static Lazy<CurrentSession> _instance = new Lazy<CurrentSession>(() => new CurrentSession());
        public static CurrentSession Instance { get { return _instance.Value; } }

        private CurrentSession() { }

    }
}
