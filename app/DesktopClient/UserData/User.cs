﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DesktopClient.UserData
{
    public class User
    {
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string Password { get; set; }
        public decimal Profits { get; set; }

        public User(string userName, string password, string email = "")
        {
            UserName = userName;
            UserEmail = email;
            Password = password;
            Profits = 0;
        }
    }
}
