﻿using DesktopClient.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.Metadata;
using System.Text;

namespace DesktopClient.UserData
{
    class UserAuthenticationProvider
    {
        static public bool CheckUserCreditentials(User user)
        {
            var filePath = FilePaths.GetUserCreditentialsFilePath();
            var jsonData = File.ReadAllText(filePath);
            var userList = JsonConvert.DeserializeObject<List<User>>(jsonData)
                        ?? new List<User>();
            var foundUser = userList.Find(usr => usr.UserName.Equals(user.UserName));
            return foundUser.Password == user.Password;
        }
        static public bool AreFieldsCompleted(User user)
        {
            return !string.IsNullOrEmpty(user.UserName)
                && !string.IsNullOrEmpty(user.Password)
                && !string.IsNullOrEmpty(user.UserEmail);
        }
        static public bool UserExists(User user)
        {
            var filePath = FilePaths.GetUserCreditentialsFilePath();
            if (File.Exists(filePath))
            {
                string jsonData = File.ReadAllText(filePath);
                var userList = JsonConvert.DeserializeObject<List<User>>(jsonData)
                ?? new List<User>();
                if(userList != null)
                    return userList.Exists(usr => usr.UserName.Equals(user.UserName));
            }
            return false;
        }
        static public void SerializeUserCreditentials(User user)
        {
            var dirName = Constants.UserDataDirName;
            if (!Directory.Exists(dirName))
                Directory.CreateDirectory(dirName);
            var filePath = FilePaths.GetUserCreditentialsFilePath(); ;

            string jsonData = null;
            List<User> userList = null;
            try
            {
                if (File.Exists(filePath))
                {
                    jsonData = File.ReadAllText(filePath);
                    userList = JsonConvert.DeserializeObject<List<User>>(jsonData)
                                ?? new List<User>();
                }
                else
                {
                    userList = new List<User>();
                }
                userList.Add(user);
                jsonData = JsonConvert.SerializeObject(userList);
                File.WriteAllText(filePath, jsonData);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
