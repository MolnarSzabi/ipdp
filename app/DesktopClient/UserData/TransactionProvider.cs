﻿using DesktopClient.Financials;
using DesktopClient.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Controls;

namespace DesktopClient.UserData
{
    class TransactionProvider
    {

        static public void SerializeTransactions(UserTransaction transaction)
        {
            var dirName = Constants.UserDataDirName;
            if (!Directory.Exists(dirName))
                Directory.CreateDirectory(dirName);
            var filePath = FilePaths.GetUserTransactionsFilePath(); ;

            string jsonData = null;
            List<UserTransaction> transactionList = null;
            try
            {
                if (File.Exists(filePath))
                {
                    jsonData = File.ReadAllText(filePath);
                    transactionList = JsonConvert.DeserializeObject<List<UserTransaction>>(jsonData)
                                ?? new List<UserTransaction>();
                }
                else
                {
                    transactionList = new List<UserTransaction>();
                }

                var foundTransaction = transactionList.Find(trans => trans.Ticker.Equals(transaction.Ticker) &&
                                                 trans.UserName.Equals(transaction.UserName));
                if (foundTransaction != null) { 
                    foundTransaction.AvgOpeningPrice = (foundTransaction.AvgOpeningPrice * foundTransaction.Volume +
                        transaction.AvgOpeningPrice * transaction.Volume) / (foundTransaction.Volume + transaction.Volume);
                    foundTransaction.Volume += transaction.Volume;
                    foundTransaction.Profit = (transaction.MarketPrice - foundTransaction.AvgOpeningPrice) * foundTransaction.Volume;
                }
                else transactionList.Add(transaction);
                jsonData = JsonConvert.SerializeObject(transactionList);
                File.WriteAllText(filePath, jsonData);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static public List<UserTransaction> GetOpenPositionsList()
        {
            var dirName = Constants.UserDataDirName;
            if (!Directory.Exists(dirName))
                Directory.CreateDirectory(dirName);
            var filePath = FilePaths.GetUserTransactionsFilePath(); ;

            string jsonData = null;
            List<UserTransaction> transactionList = null;

            if (File.Exists(filePath))
            {
                jsonData = File.ReadAllText(filePath);
                transactionList = JsonConvert.DeserializeObject<List<UserTransaction>>(jsonData)
                            ?? new List<UserTransaction>();
            }

            return transactionList;
        }

        static public UserTransaction GetUserTransactionsObject(string ticker, int volume)
        {
            try
            {
                var userName = CurrentSession.Instance.User.UserName;
                IntrinioStockDataProvider stockDataProvider = new IntrinioStockDataProvider();
                var stockData = stockDataProvider.GetSecurityRealtimePrice(ticker, IntrinioSources.Iex);
                var price = stockData.LastPrice.Value;
                return new UserTransaction(userName, ticker, price, price, 0, volume);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        static public string CalculateProfits()
        {
            decimal profits = 0;
            var currentUser = CurrentSession.Instance.User;
            var transactions = GetOpenPositionsList();
            foreach (var transaction in transactions)
                if (transaction.UserName.Equals(currentUser.UserName))
                    profits += transaction.Profit;
            CurrentSession.Instance.User.Profits = profits;
            return profits.ToString("c");
        }
    }
}
