﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DesktopClient.UserData
{
    public class UserTransaction : INotifyPropertyChanged
    {
        private string _userName;
        public string UserName
        { 
            get => _userName; 
            set {
                _userName = value;
                OnPropertyChanged(nameof(UserName));
            }
            
        }
        private string _ticker;
        public string Ticker 
        {
            get => _ticker;
            set
            {
                _ticker = value;
                OnPropertyChanged(nameof(Ticker));
            }
        }

        private decimal _marketPrice;
        public decimal MarketPrice 
        {
            get => _marketPrice;
            set
            {
                _marketPrice = value;
                OnPropertyChanged(nameof(MarketPrice));
            }
        }
        private decimal _avgOpeningPrice;
        public decimal AvgOpeningPrice
        {
            get => _avgOpeningPrice;
            set
            {
                _avgOpeningPrice = value;
                OnPropertyChanged(nameof(AvgOpeningPrice));
            }
        }
        private decimal _profit;
        public decimal Profit
        {
            get => _profit;
            set
            {
                _profit = value;
                OnPropertyChanged(nameof(Profit));
            }
        }
        private int _volume;
        public int Volume
        {
            get => _volume;
            set
            {
                _volume = value;
                OnPropertyChanged(nameof(Volume));
            }
        }

        public UserTransaction(string username, string ticker, decimal marketPrice, decimal openingPrice, decimal profit, int volume) 
        {
            UserName = username;
            Ticker = ticker;
            MarketPrice = marketPrice;
            AvgOpeningPrice = openingPrice;
            Profit = profit;
            Volume = volume;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
