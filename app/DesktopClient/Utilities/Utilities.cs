﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DesktopClient.Utilities
{
    public static class Constants
    {
        public const int OneDay = 1;
        public const int TwoDay = 2;
        public const int ThreeDay = 3;
        public const int ClosingHour = 20;
        public const int ClosingMinute = 0;
        public const int ClosingSecond = 0;
        public const int I_OpeningHour = 13;
        public const int OpeningMinute = 0;
        public const int OpeningSecond = 0;
        public const int IntradayMaxPageCount = 5;
        public const int StockSecurityPageSize = 5;
        public const int IntrinioChartUpdateSeconds = 30;

        public const double HalfHour = 0.5;
        public const double D_OpeningHour = 13.5;

        public const string UserDataDirName = "data";
    }

    public static class FilePaths
    {
        static public string GetUserTransactionsFilePath()
        {
            const string FileName = "UserTransactions.json";
            return Path.Combine(Constants.UserDataDirName, FileName);
        }

        static public string GetUserCreditentialsFilePath()
        {
            const string FileName = "UserCreditentials.json";
            return Path.Combine(Constants.UserDataDirName, FileName);
        }
    }

    public static class IntrinioSources
    {
        public const string Iex  = "iex";
        public const string Bats = "bats";
    }
}



