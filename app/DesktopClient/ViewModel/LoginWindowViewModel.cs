﻿using DesktopClient.UserData;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Controls;

namespace DesktopClient.ViewModel
{
    class LoginWindowViewModel 
    {
        public string UserName { get; set; }

        public bool DoLogin(string password)
        {
            var user = new User(UserName, password);
            if (UserAuthenticationProvider.UserExists(user))
            {
                CurrentSession.Instance.User = user;
                return (UserAuthenticationProvider.CheckUserCreditentials(user) == true);
            }
            return false;
        }
    }
}
