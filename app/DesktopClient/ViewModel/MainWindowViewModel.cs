﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using LiveCharts;
using DesktopClient.AlphaVDataProvider;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Threading;
using AlphaVantage.Net.Stocks.TimeSeries;
using LiveCharts.Wpf;
using LiveCharts.Geared;
using DesktopClient.Financials;
using System.Threading.Tasks;
using Intrinio.SDK.Model;
using ServiceStack;
using System.Media;
using Intrinio.SDK.Client;
using System.Data;
using Constants = DesktopClient.Utilities.Constants;
using IntrinioSources = DesktopClient.Utilities.IntrinioSources;
using System.Collections.ObjectModel;
using DesktopClient.UserData;

namespace DesktopClient.ViewModel
{
    public partial class MainWindowViewModel : INotifyPropertyChanged
    {

        private SeriesCollection _seriesCollection;
        public SeriesCollection SeriesCollection
        {
            get => _seriesCollection;
            set
            {
                _seriesCollection = value;
                OnPropertyChanged(nameof(SeriesCollection));
            }
        }

        private List<string> _labels;
        public List<string> Labels
        {
            get => _labels;
            set
            {
                _labels = value;
                OnPropertyChanged(nameof(Labels));
            }
        }

        private ObservableCollection<CustomStockDataPoint> _items;
        public ObservableCollection<CustomStockDataPoint> Items
        {
            get => _items;
            set
            {
                _items = value;
                OnPropertyChanged(nameof(Items));
            }
        }
        private ObservableCollection<UserTransaction> _openPositionItems;
        public ObservableCollection<UserTransaction> OpenPositionItems
        {
            get => _openPositionItems;
            set
            {
                _openPositionItems = value;
                OnPropertyChanged(nameof(OpenPositionItems));
            }
        }

        private Func<double, string> _yFormatter;
        public Func<double, string> YFormatter
        {
            get => _yFormatter;
            set
            {
                _yFormatter = value;
                OnPropertyChanged(nameof(YFormatter));
            }
        }
        public bool UseIntrinioApi { get; set; }

        private AlphaVantageStockDataProvider stockDataAlpha;
        private IntrinioStockDataProvider stockDataIntrinio;
        private readonly HashSet<string> consumeableStockStringList;
        private readonly HashSet<string> checkedStockStringList;
        private readonly List<StockTimeSeries> stockTimeSeriesList;
        private readonly List<ApiResponseSecurityStockPrices> stockApiResponsePricesList;
        private readonly List<StockPriceSummary> stockPriceSummaryList;
        private Timer aplhaVintageRealTimeChartUpdater;
        private Timer StockGenerator;
        private readonly List<Timer> intrinioRealTimeStockGenerator;
        private Timer intrinioRealTimeChartUpdater;

        public MainWindowViewModel()
        {
            stockDataAlpha = new AlphaVantageStockDataProvider();
            stockDataIntrinio = new IntrinioStockDataProvider();
            stockTimeSeriesList = new List<StockTimeSeries>();
            stockApiResponsePricesList = new List<ApiResponseSecurityStockPrices>();
            intrinioRealTimeStockGenerator = new List<Timer>();
            stockPriceSummaryList = new List<StockPriceSummary>();
            checkedStockStringList = new HashSet<string>();
            Items = new ObservableCollection<CustomStockDataPoint>();
            OpenPositionItems = new ObservableCollection<UserTransaction>();
            consumeableStockStringList = new HashSet<string>{
              "AAPL",
              "CAT",
              "BA",
              "WMT",
              "IBM",
              "XOM",
              "AXP",
              "PFE",
              "NKE",
              "MCD",
              "JNJ",
              "HD",
              "KO",
              "MMM",
              "MSFT",
              "V",
              "CVX",
              "CSCO",
              "GS",
              "INTC",
              "JPM",
              "MRK",
              "PG",
              "UNH",
              "TRV"
            };
           
            UseIntrinioApi = true;

            if (!UseIntrinioApi)
            {
                StockGenerator = new Timer(
                    async e => await GenerateStockListAsync(),
                    null,
                    TimeSpan.FromSeconds(0),
                    TimeSpan.FromMinutes(1));
            }
            else
            {
                Task.Run(async () =>
                {
                    LoadOpenPositions();
                    await GenerateStockListIntrinio();
                });
            }
        }
        void LoadOpenPositions()
        {
            var currentUsername = CurrentSession.Instance.User.UserName;
            var transactsionsList = TransactionProvider.GetOpenPositionsList();
            if (transactsionsList != null)
            {
                foreach (var transaction in transactsionsList)
                    if (transaction.UserName == currentUsername)
                        AddTransactionToDataGrid(transaction);
            }
        }
        public async Task GenerateStockListIntrinio()
        {
            System.Diagnostics.Stopwatch watch;
            try
            {
                foreach (string ticker in consumeableStockStringList)
                {
                    watch = System.Diagnostics.Stopwatch.StartNew();
                    var stockData = GetSecurityHistoricalData(ticker, "daily");
                    stockApiResponsePricesList.Add(stockData);
                    AddStockToDataGrid(stockData);
                    StartRealTimeData(stockData);
                    watch.Stop();
                    decimal elapsedSec = watch.ElapsedMilliseconds / 1000;
                    Console.WriteLine("It took:" + elapsedSec + "s to display " + stockData.Security.Ticker + "'s data!");
                }
                if (consumeableStockStringList.IsEmpty())
                {
                    Console.WriteLine("Checked whole list!");
                    await StopStockGeneration();
                }
            }
            catch (ApiException e)
            {
                PlayErrorSound();
                if (e.ErrorCode == 400)
                {
                    Console.WriteLine($"Error code: {e.ErrorCode}\n{e.Message}");
                    Environment.Exit(1); 
                }
                if (e.ErrorCode == 404)
                {
                    Console.WriteLine($"Error code: {e.ErrorCode}, the endpoint requested does not exist");
                    Environment.Exit(1);
                }
                if (e.ErrorCode == 500 || e.ErrorCode == 503)
                {
                    Console.WriteLine($"Error code: {e.ErrorCode}, server error or unavailable service");
                    await ResumeStockGeneration();
                }
                if (e.ErrorCode == 502)
                {
                    Console.WriteLine($"Error code: {e.ErrorCode}, bad gateway");
                    await ResumeStockGeneration();
                }
                if (e.ErrorCode == 429)
                {
                    Console.WriteLine($"Error code: {e.ErrorCode}, request limit has been exceeded!");
                    await ResumeStockGeneration();
                }
            }
            catch (Exception e)
            {
                PlayErrorSound();
                Console.WriteLine(e.Message);
            }
        }
        void StartRealTimeData(ApiResponseSecurityStockPrices stock)
        {
            if (IsStockMarketPastOpeningPhase() && !IsStockMarketClosed())
            {
                intrinioRealTimeStockGenerator.Add(new Timer(
                   e => AddRealTimeData(stock),
                   null,
                   TimeSpan.FromSeconds(3),
                   TimeSpan.FromSeconds(30)
                   ));
            }
        }
        void AddRealTimeData(ApiResponseSecurityStockPrices stock)
        {
            try
            {
                var ticker = stock.Security.Ticker;
                var realTimePrice = stockDataIntrinio.GetSecurityRealtimePrice(ticker, IntrinioSources.Iex);
                UpdateDataGridRealTime(ticker, realTimePrice);
            }
            catch (ApiException e)
            {
                if(e.ErrorCode == 404)
                Console.WriteLine(e.ErrorCode + " " + e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
        void UpdateDataGridRealTime(string ticker, RealtimeStockPrice realTimePrice)
        {
            var customStock = Items.ToList().Find(x => x.Symbol.Equals(ticker));
            customStock.Price = realTimePrice.LastPrice ?? 0;
            customStock.TradingTime = realTimePrice.UpdatedOn ?? default(DateTime);
            customStock.SetPrecisionTo(2);
            Console.WriteLine(ticker + " on datagrid has been updated!");
        }
        ApiResponseSecurityStockPrices GetSecurityHistoricalData(string ticker, string frequency)
        {
            var stockData = stockDataIntrinio.GetSecurityStockPrices(ticker, frequency: frequency);
            var nextPage = stockData.NextPage;
            int count = 1;
            while (nextPage != null && count < Constants.StockSecurityPageSize)
            {
                var nextStockData = stockDataIntrinio.GetSecurityStockPrices(ticker, nextPage: nextPage);
                foreach (var stock in nextStockData.StockPrices)
                    stockData.StockPrices.Add(stock);
                nextPage = nextStockData.NextPage;
                Console.WriteLine(stockData.Security.Name + " stock data points:" + stockData.StockPrices.Count);
                count++;
            }
            stockData.StockPrices.Reverse();
            
            var dayOfWeek = DateTime.UtcNow.DayOfWeek;
            if (dayOfWeek == DayOfWeek.Saturday)
            {
                var utcTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day - Constants.OneDay,
                    Constants.ClosingHour, Constants.ClosingMinute, Constants.ClosingSecond);
                AddIntradayData(utcTime, ticker, ref stockData);
            }
            else if (dayOfWeek == DayOfWeek.Sunday)
            {
                var utcTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day - Constants.TwoDay, 
                    Constants.ClosingHour, Constants.ClosingMinute, Constants.ClosingSecond);
                AddIntradayData(utcTime, ticker, ref stockData);
            }
            else if (!IsStockMarketPastOpeningPhase() && dayOfWeek == DayOfWeek.Monday)
            {
                var utcTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day - Constants.ThreeDay, 
                    Constants.ClosingHour, Constants.ClosingMinute, Constants.ClosingSecond);
                AddIntradayData(utcTime, ticker, ref stockData);
            }
            else if (IsStockMarketPastOpeningPhase() && !IsStockMarketClosed()) 
            {
                var utcTime = DateTime.UtcNow;
                AddIntradayData(utcTime, ticker, ref stockData);
            }
            else
            {
                var utcTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day,
                    Constants.ClosingHour, Constants.ClosingMinute, Constants.ClosingSecond);
                AddIntradayData(utcTime, ticker, ref stockData);
            }

            foreach (var stock in stockPriceSummaryList)
                stockData.StockPrices.Add(stock);
            return stockData;
        }
        void AddStockToDataGrid(ApiResponseSecurityStockPrices stockData)
        {
            var customStock = GetStockAsCustomStockData(stockData);
            checkedStockStringList.Add(customStock.Symbol);
            AddCustomStockToDataGrid(customStock);
            Console.WriteLine(customStock.Symbol);
        }

        //didn't succeed to use it - something isn't working correctly yet
        public CustomStockDataPoint GetStockAsCustomStockData(RealtimeStockPrice stockData)
        {
            try
            {
                var customStockData = new CustomStockDataPoint()
                {
                    Symbol = stockData.Security.Ticker,
                    Price = stockData.LastPrice ?? 0,
                    HighestPrice = stockData.HighPrice ?? 0,
                    LowestPrice = stockData.LowPrice ?? 0,
                    TradingTime = stockData.UpdatedOn ?? default(DateTime)//,
                 //   PercentageChange = calculatePercenageChange(stockData.OpenPrice??0, stockData.LastPrice??0)
                };

                customStockData.SetPrecisionTo(2);
                return customStockData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        public CustomStockDataPoint GetStockAsCustomStockData(ApiResponseSecurityStockPrices stockData)
        {
            try
            {
                var lastStockData = stockData.StockPrices.OfType<StockPriceSummary>().LastOrDefault();
                var customStockData = new CustomStockDataPoint()
                {
                    Symbol = stockData.Security.Ticker,
                    Price = lastStockData.AdjClose ?? 0,
                    HighestPrice = lastStockData.AdjHigh ?? 0,
                    LowestPrice = lastStockData.AdjLow ?? 0,
                    TradingTime = lastStockData.Date ?? default(DateTime)
                };

                customStockData.SetPrecisionTo(2);
                return customStockData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
        void AddCustomStockToDataGrid(CustomStockDataPoint customStock)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(() => {
                Items.Add(customStock);
            });
        }

        public void AddTransactionToDataGrid(UserTransaction transaction)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(() => {
                var foundTransaction = OpenPositionItems.ToList().Find(pos => pos.Ticker.Equals(transaction.Ticker));
                if (foundTransaction != null)
                {
                    if(transaction.Volume < 0 && (foundTransaction.Volume + transaction.Volume) <= 0) // sell
                        OpenPositionItems.Remove(foundTransaction);
                    else if(foundTransaction.Volume > 0)  //buy
                        CalculatePosition(transaction, foundTransaction);
                }
                else if(transaction.Volume > 0) //buy if no transaction found
                    OpenPositionItems.Add(transaction);
            });
        }

        void CalculatePosition(UserTransaction newTransaction, UserTransaction previousTransaction)
        {
            previousTransaction.AvgOpeningPrice = (previousTransaction.AvgOpeningPrice * previousTransaction.Volume +
                newTransaction.AvgOpeningPrice * newTransaction.Volume) / (previousTransaction.Volume + newTransaction.Volume);
            previousTransaction.Volume += newTransaction.Volume;
            previousTransaction.Profit = (previousTransaction.MarketPrice - newTransaction.AvgOpeningPrice) * previousTransaction.Volume;
        }
        void AddIntradayData(DateTime endDate, string ticker, ref ApiResponseSecurityStockPrices stockData)
        {
            DateTime startDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, Constants.I_OpeningHour, Constants.OpeningMinute, Constants.OpeningSecond);
            int pageCount = 0;
            int intradayPageSize = 1;
            while (startDate <= endDate && pageCount < Constants.IntradayMaxPageCount)
            {
                var intradayStockPrices = stockDataIntrinio.GetSecurityIntradayPrices(ticker, IntrinioSources.Iex, startDate,"WHATEVER", startDate.AddHours(Constants.HalfHour), "WHATEVER", intradayPageSize);
                foreach (var stock in intradayStockPrices.IntradayPrices)
                    stockData.StockPrices.Add(new StockPriceSummary(stock.Time) { AdjClose = stock.LastPrice });
                startDate =  startDate.AddHours(Constants.HalfHour);
                Console.WriteLine("Checked data until: " + startDate +" page count: " + pageCount);
                pageCount++;
            }
        }

        //to be used only with the right data - yesterday closing price and today's closing price 
        string calculatePercenageChange(decimal oldPrice, decimal newPrice)
        {
            try
            {
                decimal change = 0;
                if (newPrice > oldPrice)
                    change = (newPrice - oldPrice) / oldPrice * 100;
                else
                    change = (oldPrice - newPrice) / oldPrice * 100;

                return Math.Round(change, 2, MidpointRounding.ToEven).ToString() + "%";

            }
            catch (DivideByZeroException)
            {
                return "0";
            }
        }
        async Task GenerateStockListAsync()
        {
            try
            {
                int count = 0;
                foreach (string ticker in consumeableStockStringList)
                {
                    var stockTimeSeries = await stockDataAlpha.GetIntradayTimeSeriesAsync(ticker);
                    stockTimeSeriesList.Add(stockTimeSeries);
                    var lastStockData = stockTimeSeries.DataPoints.OfType<StockDataPoint>().FirstOrDefault();
                    CustomStockDataPoint customStock = new CustomStockDataPoint()
                    {
                        Symbol = stockTimeSeries.Symbol,
                        Price = lastStockData.ClosingPrice,
                        HighestPrice = lastStockData.HighestPrice,
                        LowestPrice = lastStockData.LowestPrice
                    };

                    customStock.SetPrecisionTo(2);
                    AddCustomStockToDataGrid(customStock);
                    Console.WriteLine(stockTimeSeries.Symbol);

                    count++;
                    checkedStockStringList.Add(ticker);
                    if (count == 4)
                        break;
                }

                RemoveConsumedStocks();
                if (consumeableStockStringList.IsEmpty())
                {
                    Console.WriteLine("Checked list!");
                    await StopStockGeneration();
                }
            }
            catch (Exception e)
            {
                PlayErrorSound();
                RemoveConsumedStocks();
                Console.WriteLine("Called too soon, request limit bay have been exceeded.");
                Console.WriteLine(e.Message);
            }
        }

        async Task ResumeStockGeneration()
        {
            RemoveConsumedStocks();
            await Task.Run(async () =>
            {
                Thread.Sleep(1000);
                await GenerateStockListIntrinio();
            });
        }

        #region commented parallel stock generation block

        //try
        //{
        //    var getStockTimeSeries = new TransformBlock<string, StockTimeSeries>(
        //        async ticker =>
        //        {
        //            var stockTimeSeries = await stockData.GetDailyTimeSeriesAsync(ticker);
        //            stockTimeSeriesList.Add(stockTimeSeries);

        //            var lastStockData = stockTimeSeries.DataPoints.OfType<StockDataPoint>().FirstOrDefault();

        //            CustomStockDataPoint customStock = new CustomStockDataPoint()
        //            {
        //                Symbol = stockTimeSeries.Symbol,
        //                Price = lastStockData.ClosingPrice,
        //                HighestPrice = lastStockData.HighestPrice,
        //                LowestPrice = lastStockData.LowestPrice
        //            };

        //            customStock.SetPrecisionTo(2); 
        //            _items.Add(customStock);

        //            Items = new List<CustomStockDataPoint>(_items);

        //            return stockTimeSeries;

        //        }, new ExecutionDataflowBlockOptions
        //        {
        //            MaxDegreeOfParallelism = 1
        //        });

        //    var writeCustomerBlock = new ActionBlock<StockTimeSeries>(c => Console.WriteLine(c.Symbol));

        //    getStockTimeSeries.LinkTo(
        //        writeCustomerBlock, new DataflowLinkOptions
        //        {
        //            PropagateCompletion = true
        //        });

        //    int count = 0;
        //    foreach (var stock in stockStringList)
        //    {
        //        var res = getStockTimeSeries.Post(stock);
        //        Console.WriteLine(stock + " post is " + res + ", count: " + count);
        //        count++;
        //        checkedStockStringList.Add(stock);
        //        if (count == 5)
        //            break;
        //    }
        //    getStockTimeSeries.Complete();
        ////    writeCustomerBlock.Completion.Wait();

        //    foreach (var stock in checkedStockStringList)
        //        if (stockStringList.Contains(stock))
        //        {
        //            stockStringList.Remove(stock);
        //            Console.WriteLine(stock + " has been removed.");
        //        }

        //    checkedStockStringList.Clear();
        //    Console.WriteLine("Refresh?");

        //}
        //catch (NullReferenceException)
        //{
        //    Console.WriteLine("Data limit excedeed when generating stock list!");
        //}
        //catch (Exception e)
        //{
        //    Console.WriteLine("Error occured when generating stock list! Check parallel stock generation.");
        //    Console.WriteLine(e.Message);
        //}
        //} //end of bracket
       
        #endregion
        public void RemoveConsumedStocks()
        {
            foreach (var stock in checkedStockStringList)
                if (consumeableStockStringList.Contains(stock))
                {
                    consumeableStockStringList.Remove(stock);
                    Console.WriteLine(stock + " has been removed from consumable list.");
                }
            checkedStockStringList.Clear();
        }

        public void PlayErrorSound() => SystemSounds.Hand.Play();

        public async Task StopStockGeneration()
        {
            if (StockGenerator != null)
                await StockGenerator.DisposeAsync();
            Console.WriteLine("Generation done");
        }

        public bool IsStockMarketPastOpeningPhase()
        {
            var today = DateTime.UtcNow;
            if (today.ToString("tt") == "PM")
            {
                if (today.TimeOfDay.TotalHours >= Constants.D_OpeningHour)
                    return true;
                else 
                    return false;
            }
            return false;
        }

        bool IsWeekend()
        {
            var dayOfWeek = DateTime.UtcNow.DayOfWeek;
            return (dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday);
        }

        public bool IsStockMarketClosed()
        {
            var today = DateTime.UtcNow;
            if (today.ToString("tt") == "PM" && today.Hour > Constants.ClosingHour)
                return true;
            return false;
        }

        public async Task StopChartUpdate()
        {
            if(aplhaVintageRealTimeChartUpdater != null)
                await aplhaVintageRealTimeChartUpdater.DisposeAsync();
            if (intrinioRealTimeChartUpdater != null)
                await intrinioRealTimeChartUpdater.DisposeAsync();
        }

        public StockTimeSeries FindStockDataAlphaVintage(string ticker)
        {
           return stockTimeSeriesList.Find(x => x.Symbol.Contains(ticker));
        } 
        public ApiResponseSecurityStockPrices FindStockDataIntrinio(string ticker)
        {
           return stockApiResponsePricesList.Find(x => x.Security.Ticker.Equals(ticker));
        }

        public void PlotData(StockTimeSeries stock)
        {
            string ticker = stock.Symbol.ToString();

            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = ticker,
                    Values = new GearedValues<decimal>().AsGearedValues().WithQuality(Quality.Low)
                }
            };

            Labels = new List<string>();
            var ordered = stock.DataPoints.OrderBy(dataPoint => dataPoint.Time).ToList();
            foreach (var data in ordered)
            {
                SeriesCollection[0].Values.Add(data.ClosingPrice);
                Labels.Add(data.Time.ToString("dd MMM yyyy hh:mm:ss"));    
            }

            YFormatter = value => value.ToString("C");
        }

        public void PlotData(ApiResponseSecurityStockPrices stock)
        {
            string ticker = stock.Security.Ticker;

            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = ticker,
                    Values = new GearedValues<decimal>().AsGearedValues().WithQuality(Quality.Low)
                }
            };

            Labels = new List<string>();
            var ordered = stock.StockPrices.OrderBy(dataPoint => dataPoint.Date).ToList();
            foreach (var stockData in ordered)
            {
                SeriesCollection[0].Values.Add(stockData.AdjClose);
                Labels.Add(stockData.Date?.ToString("dd MMM yyyy hh:mm:ss"));
            }

            YFormatter = value => value.ToString("C");
        }
        public CustomStockDataPoint RequestLastestAlphaVintageCustomStockData(string ticker, int hourDifferance = 7)
        {
            CustomStockDataPoint lastestStockData = null;
            TimeSpan americanTimeZoneDifferance = TimeSpan.FromHours(hourDifferance);
            try
            {
                var lastStockData = stockDataAlpha.GetEndQuoteTimeSeries(ticker);
                var parsedStockData = JObject.Parse(lastStockData);
                var stockPrice = Convert.ToDecimal(parsedStockData["Global Quote"]["05. price"]);
                var high = Convert.ToDecimal(parsedStockData["Global Quote"]["03. high"]);
                var low = Convert.ToDecimal(parsedStockData["Global Quote"]["04. low"]);
                var changePercentage = parsedStockData["Global Quote"]["10. change percent"].ToString();
                var tradingTime = DateTime.Parse(parsedStockData["Global Quote"]["07. latest trading day"].ToString() + DateTime.Now.Subtract(americanTimeZoneDifferance).ToString(" hh:mm:ss"));

                lastestStockData = new CustomStockDataPoint()
                {
                    Symbol = ticker,
                    Price = stockPrice,
                    HighestPrice = high,
                    LowestPrice = low,
                    PercentageChange = changePercentage,
                    TradingTime = tradingTime
                };
            }
            catch (NullReferenceException)
            {
                throw;
            }

        return lastestStockData;
        }
        public void UpdateDataGridPartially(string latestStockTicker, object selectedItem)
        {
            Console.WriteLine("Checking for update");
            try
            {
                int hourDifferance = 3;
                var customStockData = RequestLastestAlphaVintageCustomStockData(latestStockTicker, hourDifferance);
                if (selectedItem is CustomStockDataPoint customStockDataPoint)
                {
                    customStockDataPoint.LowestPrice = Math.Round(customStockData.LowestPrice, 2, MidpointRounding.ToEven);
                    customStockDataPoint.HighestPrice = Math.Round(customStockData.HighestPrice, 2, MidpointRounding.ToEven);
                    customStockDataPoint.PercentageChange = StringPercentageFormatter(customStockData.PercentageChange);
                }
                Console.WriteLine("Updated chart");
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Data limit excedeed.");
            }
        }
        public void StartWindowDataUpdaterAlphaVantage(string latestStockTicker, object selectedItem)
        {
                aplhaVintageRealTimeChartUpdater = new Timer(
                    e => 
                    {
                        Console.WriteLine("Checking for update");
                        CustomStockDataPoint customStockData = null;
                        try
                        {
                             customStockData = RequestLastestAlphaVintageCustomStockData(latestStockTicker);
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("Data limit excedeed.");
                        }
                        if(customStockData != null)
                        {
                            UpdateChart(customStockData);
                            UpdateDataGrid(customStockData, selectedItem);
                            Console.WriteLine("Updated chart");
                        }
                    },
                    null,
                    TimeSpan.FromSeconds(2),
                    TimeSpan.FromMinutes(1));
        }

        public void StartIntrinioChartUpdater(string ticker, object selectedItem)
        {
            var dayOfWeek = DateTime.UtcNow.DayOfWeek;
            if(IsStockMarketPastOpeningPhase() && !IsStockMarketClosed() && !IsWeekend())
            {
                intrinioRealTimeChartUpdater = new Timer(
                    e =>
                    {
                        Console.WriteLine("Checking for update");
                        try
                        {
                            var realTimePrice = stockDataIntrinio.GetSecurityRealtimePrice(ticker, IntrinioSources.Iex);
                            UpdateChart(realTimePrice);
                            Console.WriteLine("Updated chart");
                        }
                        catch (ApiException)
                        {
                            Console.WriteLine("Data limit excedeed.");
                        }
                    },
                    null,
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromSeconds(Constants.IntrinioChartUpdateSeconds));
            }
        }

        string StringPercentageFormatter(string percentage)
        {
            int precision = 2;
            var pieces = percentage.Split('%');
            var formattedDecimal = Math.Round(Convert.ToDecimal(pieces[0]), precision);
            return $"{formattedDecimal}%";
        }
        void UpdateDataGrid(CustomStockDataPoint customStockData, object selectedItem)
        {
            try
            {
                if(selectedItem is CustomStockDataPoint customStockDataPoint)
                {
                    customStockDataPoint.Price = Math.Round(customStockData.Price, 2, MidpointRounding.ToEven);
                    customStockDataPoint.LowestPrice = Math.Round(customStockData.LowestPrice, 2, MidpointRounding.ToEven);
                    customStockDataPoint.HighestPrice = Math.Round(customStockData.HighestPrice, 2, MidpointRounding.ToEven);
                    customStockDataPoint.PercentageChange = StringPercentageFormatter(customStockData.PercentageChange);
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("UpdateDataGrid error occured!");
            }
        }

        public void UpdateChart(CustomStockDataPoint customStockData)
        {
            try
            {
                SeriesCollection[0].Values.Add(customStockData.Price);
                Labels.Add(customStockData.TradingTime.ToString("dd MMM yyyy hh:mm:ss"));
            }
            catch (Exception)
            {
                Console.WriteLine("Update Chart error occured!");
            }
        }

        public void UpdateChart(RealtimeStockPrice stock)
        {
            try
            {
                SeriesCollection[0].Values.Add(stock.LastPrice);
                Labels.Add(stock.UpdatedOn?.ToString("dd MMM yyyy hh:mm:ss")); 
            }
            catch (ApiException e)
            {
                Console.WriteLine("Update Chart error occured! Error code: " + e.ErrorCode + e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Update Chart error occured!" + e.Message);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
