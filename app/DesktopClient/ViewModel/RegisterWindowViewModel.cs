﻿using DesktopClient.UserData;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.Json;

namespace DesktopClient.ViewModel
{
    class RegisterWindowViewModel
    {
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public bool DoRegister(string password) 
        {
            var user = new User(UserName, password, UserEmail);
            if (UserAuthenticationProvider.AreFieldsCompleted(user))
            {
                if (!UserAuthenticationProvider.UserExists(user))
                {
                    UserAuthenticationProvider.SerializeUserCreditentials(user);
                    return true;
                }
                return false;
            }
            else
            {
                Console.WriteLine("Fields not completed!");
                return false;
            }
        }
    }
}
