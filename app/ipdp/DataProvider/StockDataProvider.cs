﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace ipdp.DataProvider
{
    //private static readonly HttpClient client = new HttpClient();

    public sealed class StockDataProvider
    {
        HttpClient client = SingleHttpClientInstanceController.GetInstance;

        public async Task<string> GetResponse()
        {
            var responseString = await client.GetStringAsync("http://www.example.com/recepticle.aspx");
            return responseString;
        }
    }
}
