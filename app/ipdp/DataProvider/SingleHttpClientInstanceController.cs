﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ipdp.DataProvider
{
    public class SingleHttpClientInstanceController
    {
        private static HttpClient httpClient = null;
        private SingleHttpClientInstanceController()
        {
        }
        public static HttpClient GetInstance 
        {
            get 
            {
                if (httpClient == null)
                    httpClient = new HttpClient();
                return httpClient;
            }
        }
    }
}
