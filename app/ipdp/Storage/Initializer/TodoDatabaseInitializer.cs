﻿using ipdp.Storage.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace ipdp.Storage.Initializer
{
    public static class TodoDatabaseInitializer
    {
        public static void EnsureDatabaseIsProperlyInitialized(DbContext context)
        {
            context.Database.EnsureCreated();
        }

        public static async Task EnsureDatabaseIsProperlyInitializedAsync(DbContext context)
        {
            await context.Database.EnsureCreatedAsync();
        }
    }
}
