using ipdp.DataProvider;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace ipdp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            getStock();
            CreateHostBuilder(args).Build().Run();
        }

        public static void getStock()
        {
            StockDataProvider data = new StockDataProvider();
            var a = data.GetResponse();

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
