﻿#  Stock Market Simulator

This application simulates the real stock market as it uses APIs to request real time and historical data.

# Dependencies
Two APIs were used, Intrinio 5.4.0 and AlphaVantage 1.0.2.

Uses LiveCharts.Geard to plot stock prices over the time.

# How does the application work?

In its current stage, the application uses features to work as the real stock market from Monday to Friday 13:30 to 20:00 UTC(or 16:30-23:00 GMT+3).
Chart will request data and update every 30 seconds.
Stock list in the left is being generated with requests around every second and stock price in data grid is being updated every 30 seconds after it is displayed.
Stock list in the bottom is a stock list of brought stock from the current user that is logged in.
After the session is ended brought stocks are serialized to a file and reused if user logs in again.

# How to use the application?
Register with an account.
Log in with your creditentials.
Observe stock list generation inside the data grid and logs in the console for more information.
After a stock has finished generating it will be displayed. Click on it and the chart will appear.
After you click on the stock you can obveserve that the chart will update in every 30 seconds(change IntrinioChartUpdateSeconds in Utilities.cs to change update time).
If you click on another stock on the stock list the chart will change to that one and start updating it.
If you want to buy a stock, press a stock in the left stock list and press the green buy button to buy it/red button to sell it.
After that the bottom list will update the transaction. If you want to buy more, press the button again.
Every time you press the buy button 100 stock are gonna be bought. If you press the sell button, 100 stock will be sold.
You you close the app your and your stock tranzactions will be saved and reloaded if you log in again with the same user.

NOTE: The app's chart or stock list price will not update automatically outside of the real stock market's working hour!
Change StockSecurityPageSize and IntradayMaxPageCount constants from Utilities.cs if you want to see more historical/intraday data on the chart.

# Installation

1. Clone this repo: https://gitlab.com/MolnarSzabi/ipdp.git
2. Go to app and execute the solution.
3. Set DesktopClient as startup project (ignore ipdp project, it was used as an example in the past)
4. Build and run
